/**
 * This script publish messages to the named exchange 'ex-fan' every one second.
 */

const messages = require('../messages.json')

const { RMQBuilder } = require('../../out')
const publisher = RMQBuilder.createFanoutPublisher('amqp://localhost', 'ex-fan')

// stop async execution for x seconds
const waiter = async (seconds) => new Promise(resolve => setTimeout(resolve, seconds))

async function main() {

    for (message of messages) {

        await publisher.publishMessage(message)

        console.log(`Message { ${message} } sent`)

        await waiter(1000)

    }

    await publisher.close()
}

main()
    .then(() => console.log('bye'))
    .catch(error => console.error(error))
