/**
 * This script connect a consumer to the 'ex-direct' exchange and the 'q-direct' queue and listen for the messages in the queue.
 */

const { RMQBuilder } = require('../../out')

const consumer = RMQBuilder.createConsumer('amqp://localhost', 'ex-direct', 'q-direct')

consumer.on('message', (message) => {
    console.log(`New message: ${message.content}`)
    message.ack()
})

consumer.startConsuming()
    .then(() => console.log('Callback registered - waiting for messaging'))
    .catch((error) => console.error(error))