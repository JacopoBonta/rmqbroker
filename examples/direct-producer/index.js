/**
 * This script use a DirectPublisher to send messages each seconds to a queue called 'q-direct' trought an exchange named 'ex-direct'.
 */

const { RMQBuilder } = require('../../out')
const messages = require('../messages.json')

// stop async execution for x seconds
const waiter = async (seconds) => new Promise(resolve => setTimeout(resolve, seconds))

const publisher = RMQBuilder.createDirectPublisher('amqp://localhost', 'ex-direct', 'q-direct')

async function main() {

    for (message of messages) {
        
        await publisher.publishMessage(message)
        
        console.log(`Message { ${message} } sent!`)

        await waiter(1000)

    }

    await publisher.close()
}

main()
    .then(() => console.log('bye'))
    .catch(error => console.error(error))
