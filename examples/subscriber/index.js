const { RMQBuilder } = require('../../out')

const subscriber = RMQBuilder.createSubscriber('amqp://localhost', 'ex-fan')

async function main() {

    subscriber.on('message', console.log)
    
    await subscriber.subscribe()
}

main()
    .then(() => console.log('Waiting for incoming messages'))
    .catch((error) => console.error(error))