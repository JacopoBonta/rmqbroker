const { isFatalError } = require('amqplib/lib/connection')

import amqp from 'amqplib'
import { EventEmitter } from 'events'

/**
 * Allowed exchange types
 */
enum ExchangeTypes {
    Direct = 'direct',
    Fanout = 'fanout'
}

/**
 * Queue interface rappresent a queue object used by this module
 */
interface Queue {
    name: string,
    options: amqp.Options.AssertQueue
}

/**
 * Exchange interface rappresent an exchange object used by this module
 */
interface Exchange {
    name: string,
    type: ExchangeTypes,
    options: amqp.Options.AssertExchange
}

/**
 * Message is a wrapper around the amqp.Message that holds a reference to the connected channel so the message can be easily acked
 */
class Message {
    private msg: amqp.Message
    private channel: amqp.Channel

    constructor(message: amqp.Message, channel: amqp.Channel) {
        this.msg = message
        this.channel = channel
    }

    get content(): string {
        return this.msg.content.toString()
    }

    get contentAsJson(): Object {
        return this.msg.content.toJSON()
    }

    ack() {
        this.channel.ack(this.msg)
    }
}

/**
 * RMQConnectionManager is the service that manage a connection to the broker.
 * It will try to reopen a connection if a non fatal error is caught
 */
class RMQConnectionManager {

    private host: string

    public exchange: Exchange
    public queue?: Queue

    private connection: amqp.Connection | null
    private channel: amqp.Channel | null

    constructor(host: string, exchange: Exchange, queue?: Queue) {
        this.host = host
        this.exchange = exchange
        this.queue = queue

        this.connection = null
        this.channel = null
    }

    /**
     * connect() establish a connection and assert exchange and eventually a queue
     */
    async connect() {
        this.connection = await amqp.connect(this.host)
        this.channel = await this.connection.createChannel()

        this.channel.assertExchange(this.exchange.name, this.exchange.type, this.exchange.options)

        if (this.queue) {
            await this.channel.assertQueue(this.queue.name, this.queue.options)
            // await this.channel.bindQueue(q.queue, this.exchange.name, '')
        }

        this.connection.on('error', async (error) => {
            if (isFatalError(error)) {
                throw error
            }

            // when non fatal error we try to establish a new connection
            
            this.connection = null
            this.channel = null

            await this.connect()
        })

        this.connection.on('close', async (error) => {
            if (error && isFatalError(error)) {
                throw error
            }
        })
    }

    async close(): Promise<void> {
        if (this.channel) {
            await this.channel.close()
        }
        
        if (this.connection) {
            return await this.connection.close()
        }
    }

    async getChannel(): Promise<amqp.Channel> {
        if (!this.connection) {
            await this.connect()
        }
        if (this.connection && !this.channel) {
            await this.connection.close()
        }
        return this.channel as amqp.Channel
    }
}

/**
 * MessagePublisher allows to publish a message.
 * Depending on the type of the exchange it will send message to a queue (direct exchange)
 * or publish directly to the exchange (fanout exchange).
 * If there isn't at least one subscriber attached to the exchange of type fanout all messages will be lost.
 */
class MessagePublisher {

    private connectionManager: RMQConnectionManager

    constructor(connectionManager: RMQConnectionManager) {
        this.connectionManager = connectionManager
    }

    async publishMessage(message: string): Promise<void> {
        let channel = await this.connectionManager.getChannel()
        let exchange = this.connectionManager.exchange
        let queue = this.connectionManager.queue

        // se è presente la coda allora vuol dire che sono un publisher di tipo direct
        if (queue) {
            await channel.sendToQueue(queue.name, Buffer.from(message))
        } else {
            await channel.publish(exchange.name, '', Buffer.from(message))
        }
    }

    async close(): Promise<void> {
        return await this.connectionManager.close()
    }
}

/**
 * MessageSubscriber subscribe to a given exchange and start consuming its messages as soon as they are pushed to the exchange.
 * Its is meant to be used with a fanout publisher - to exchange data in real time
 */
class MessageSubscriber extends EventEmitter {

    private connectionManager: RMQConnectionManager

    constructor(connectionManager: RMQConnectionManager) {
        super()
        this.connectionManager = connectionManager
    }

    async subscribe(): Promise<void> {
        let channel = await this.connectionManager.getChannel()

        // Collego una coda 'volatile' all'exchange
        let q = await channel.assertQueue('', { durable: false, exclusive: true })

        await channel.bindQueue(q.queue, this.connectionManager.exchange.name, '')

        channel.consume(q.queue, (message: amqp.Message | null) => {
            if (message) {
                this.emit('message', message.content.toString())
            }
        }, { noAck: true })
    }

    async close(): Promise<void> {
        return await this.connectionManager.close()
    }

}

/**
 * MessageConsumer subscribe to the given exchange and queue and start consuming messages from the queue.
 * The message is acked as soon as it is read by the client. The message consumer is meant to be use with a direct publisher.
 * Messages start flushing only when a consumer is connected, otherwise they stay in the queue.
 */
class MessageConsumer extends EventEmitter {

    private connectionManager: RMQConnectionManager

    constructor(connectionManager: RMQConnectionManager) {
        super()
        this.connectionManager = connectionManager
    }

    async startConsuming(): Promise<void> {
        let channel = await this.connectionManager.getChannel()

        // deliver 1 message and wait for ack to send next
        channel.prefetch(1)
        
        let queue = this.connectionManager.queue

        if (!queue) throw new Error('queue is not defined')

        channel.consume(queue.name, (message: amqp.Message | null) => {

            if (message) {
                let m = new Message(message, channel)

                this.emit('message', m)
            }

        }, { noAck: false })
    }

    async close(): Promise<void> {
        return await this.connectionManager.close()
    }

}

/**
 * The builder is used for the dependency injection.
 * When a publisher, a consumer or a subscriber is requested it will instatntiate a connection manager service object
 * and inject it into the requested service. It will return the instantiated service.
 */
export class RMQBuilder {

    /**
     * createFanoutPublisher() istanzia un MessagePublisher di tipo fanout che viene utilizzato per mandare messaggi ad un broker di RabbitMQ.
     * Un publisher con un exchange di tipo fanout manda i messaggi direttamente all'exchange senza assicurarsi che sia presente una coda per riceverli.
     * Se nessun subscriber si connette agganciando almeno una coda all'exchange i messaggi vengono persi
     * @param rmqHost broker host address
     * @param exchangeName the name of the exchange
     * @param options optional, exchange configuration object
     */
    static createFanoutPublisher(rmqHost: string, exchangeName: string, options?: amqp.Options.AssertExchange) {

        let exchange: Exchange = {
            name: exchangeName,
            type: 'fanout',
            options: options || { durable: false }
        } as Exchange

        let cm = new RMQConnectionManager(rmqHost, exchange)

        return new MessagePublisher(cm)
    }

    /**
     * createDirectPublisher() istanzia un MessagePublisher di tipo direct che viene utilizzato per mandare messaggi ad un broker di RabbitMQ.
     * Un publisher con un exchange di tipo direct manda i messaggi all'exchange che vengono inoltrati direttamente alla coda.
     * I messaggi pubblicati non vengono persi e cominciano a fluire dalla coda non appena un consumer viene collegato.
     * @param rmqHost broker host address
     * @param exchangeName the name of the exchange
     * @param queueName the name of the queue
     * @param exchangeOptions optional, exchange configuration object
     * @param queueOptions optional, queue configuration object
     */
    static createDirectPublisher(rmqHost: string, exchangeName: string, queueName: string, exchangeOptions?: amqp.Options.AssertExchange, queueOptions?: amqp.Options.AssertQueue) {

        let exchange: Exchange = {
            name: exchangeName,
            type: 'direct',
            options: exchangeOptions || { durable: true }
        } as Exchange

        let queue: Queue = {
            name: queueName,
            options: queueOptions || { durable: true }
        } as Queue
        
        let cm = new RMQConnectionManager(rmqHost, exchange, queue)

        return new MessagePublisher(cm)
    }

    /**
     * createSubscriber() istanzia un MessageSubscriber che rimane in ascolto per i messaggi pubblicati da un publisher fanout.
     * All'exchange Viene collegata una coda con routing key scelta dal broker e con ack automatico.
     * Ad ogni messaggio ricevuto viene emesso un evento 'message' con il contenuto del messaggio.
     * @param rmqHost broker host
     * @param exchangeName the name of the exchange
     * @param options optional, exchange configuration object
     */
    static createSubscriber(rmqHost: string, exchangeName: string, options?: amqp.Options.AssertExchange) {

        let exchange: Exchange = {
            name: exchangeName,
            type: 'fanout',
            options: options || { durable: false }
        } as Exchange

        let cm = new RMQConnectionManager(rmqHost, exchange)

        return new MessageSubscriber(cm)

    }

    /**
     * createConsumer() istanzia un MessageConsumer che legge i messaggi pubblicati nella coda di un exchange di tipo 'direct'.
     * I messaggi sono esposti tramite un iteretatr, una volta consumato viene automaticamente inviato il segnale di ack.
     * @param rmqHost broker host
     * @param exchangeName the name of the exchange
     * @param queueName the name of the queue
     * @param exchangeOptions optional, exchange configuration object
     * @param queueOptions optional, queue configuration object
     */
    static createConsumer(rmqHost: string, exchangeName: string, queueName: string, exchangeOptions?: amqp.Options.AssertExchange, queueOptions?: amqp.Options.AssertQueue) {

        let exchange: Exchange = {
            name: exchangeName,
            type: 'direct',
            options: exchangeOptions || { durable: true }
        } as Exchange

        let queue: Queue = {
            name: queueName,
            options: queueOptions || { durable: true }
        } as Queue

        let cm = new RMQConnectionManager(rmqHost, exchange, queue)

        return new MessageConsumer(cm)

    }
}