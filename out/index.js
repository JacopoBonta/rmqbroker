"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var isFatalError = require('amqplib/lib/connection').isFatalError;
var amqplib_1 = __importDefault(require("amqplib"));
var events_1 = require("events");
/**
 * Allowed exchange types
 */
var ExchangeTypes;
(function (ExchangeTypes) {
    ExchangeTypes["Direct"] = "direct";
    ExchangeTypes["Fanout"] = "fanout";
})(ExchangeTypes || (ExchangeTypes = {}));
/**
 * Message is a wrapper around the amqp.Message that holds a reference to the connected channel so the message can be easily acked
 */
var Message = /** @class */ (function () {
    function Message(message, channel) {
        this.msg = message;
        this.channel = channel;
    }
    Object.defineProperty(Message.prototype, "content", {
        get: function () {
            return this.msg.content.toString();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Message.prototype, "contentAsJson", {
        get: function () {
            return this.msg.content.toJSON();
        },
        enumerable: true,
        configurable: true
    });
    Message.prototype.ack = function () {
        this.channel.ack(this.msg);
    };
    return Message;
}());
/**
 * RMQConnectionManager is the service that manage a connection to the broker.
 * It will try to reopen a connection if a non fatal error is caught
 */
var RMQConnectionManager = /** @class */ (function () {
    function RMQConnectionManager(host, exchange, queue) {
        this.host = host;
        this.exchange = exchange;
        this.queue = queue;
        this.connection = null;
        this.channel = null;
    }
    /**
     * connect() establish a connection and assert exchange and eventually a queue
     */
    RMQConnectionManager.prototype.connect = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            var _this = this;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, amqplib_1.default.connect(this.host)];
                    case 1:
                        _a.connection = _c.sent();
                        _b = this;
                        return [4 /*yield*/, this.connection.createChannel()];
                    case 2:
                        _b.channel = _c.sent();
                        this.channel.assertExchange(this.exchange.name, this.exchange.type, this.exchange.options);
                        if (!this.queue) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.channel.assertQueue(this.queue.name, this.queue.options)
                            // await this.channel.bindQueue(q.queue, this.exchange.name, '')
                        ];
                    case 3:
                        _c.sent();
                        _c.label = 4;
                    case 4:
                        this.connection.on('error', function (error) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (isFatalError(error)) {
                                            throw error;
                                        }
                                        // when non fatal error we try to establish a new connection
                                        this.connection = null;
                                        this.channel = null;
                                        return [4 /*yield*/, this.connect()];
                                    case 1:
                                        _a.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        this.connection.on('close', function (error) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                if (error && isFatalError(error)) {
                                    throw error;
                                }
                                return [2 /*return*/];
                            });
                        }); });
                        return [2 /*return*/];
                }
            });
        });
    };
    RMQConnectionManager.prototype.close = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.channel) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.channel.close()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        if (!this.connection) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.connection.close()];
                    case 3: return [2 /*return*/, _a.sent()];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    RMQConnectionManager.prototype.getChannel = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!this.connection) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.connect()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        if (!(this.connection && !this.channel)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.connection.close()];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4: return [2 /*return*/, this.channel];
                }
            });
        });
    };
    return RMQConnectionManager;
}());
/**
 * MessagePublisher allows to publish a message.
 * Depending on the type of the exchange it will send message to a queue (direct exchange)
 * or publish directly to the exchange (fanout exchange).
 * If there isn't at least one subscriber attached to the exchange of type fanout all messages will be lost.
 */
var MessagePublisher = /** @class */ (function () {
    function MessagePublisher(connectionManager) {
        this.connectionManager = connectionManager;
    }
    MessagePublisher.prototype.publishMessage = function (message) {
        return __awaiter(this, void 0, void 0, function () {
            var channel, exchange, queue;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connectionManager.getChannel()];
                    case 1:
                        channel = _a.sent();
                        exchange = this.connectionManager.exchange;
                        queue = this.connectionManager.queue;
                        if (!queue) return [3 /*break*/, 3];
                        return [4 /*yield*/, channel.sendToQueue(queue.name, Buffer.from(message))];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 3: return [4 /*yield*/, channel.publish(exchange.name, '', Buffer.from(message))];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    MessagePublisher.prototype.close = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connectionManager.close()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return MessagePublisher;
}());
/**
 * MessageSubscriber subscribe to a given exchange and start consuming its messages as soon as they are pushed to the exchange.
 * Its is meant to be used with a fanout publisher - to exchange data in real time
 */
var MessageSubscriber = /** @class */ (function (_super) {
    __extends(MessageSubscriber, _super);
    function MessageSubscriber(connectionManager) {
        var _this = _super.call(this) || this;
        _this.connectionManager = connectionManager;
        return _this;
    }
    MessageSubscriber.prototype.subscribe = function () {
        return __awaiter(this, void 0, void 0, function () {
            var channel, q;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connectionManager.getChannel()
                        // Collego una coda 'volatile' all'exchange
                    ];
                    case 1:
                        channel = _a.sent();
                        return [4 /*yield*/, channel.assertQueue('', { durable: false, exclusive: true })];
                    case 2:
                        q = _a.sent();
                        return [4 /*yield*/, channel.bindQueue(q.queue, this.connectionManager.exchange.name, '')];
                    case 3:
                        _a.sent();
                        channel.consume(q.queue, function (message) {
                            if (message) {
                                _this.emit('message', message.content.toString());
                            }
                        }, { noAck: true });
                        return [2 /*return*/];
                }
            });
        });
    };
    MessageSubscriber.prototype.close = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connectionManager.close()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return MessageSubscriber;
}(events_1.EventEmitter));
/**
 * MessageConsumer subscribe to the given exchange and queue and start consuming messages from the queue.
 * The message is acked as soon as it is read by the client. The message consumer is meant to be use with a direct publisher.
 * Messages start flushing only when a consumer is connected, otherwise they stay in the queue.
 */
var MessageConsumer = /** @class */ (function (_super) {
    __extends(MessageConsumer, _super);
    function MessageConsumer(connectionManager) {
        var _this = _super.call(this) || this;
        _this.connectionManager = connectionManager;
        return _this;
    }
    MessageConsumer.prototype.startConsuming = function () {
        return __awaiter(this, void 0, void 0, function () {
            var channel, queue;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connectionManager.getChannel()
                        // deliver 1 message and wait for ack to send next
                    ];
                    case 1:
                        channel = _a.sent();
                        // deliver 1 message and wait for ack to send next
                        channel.prefetch(1);
                        queue = this.connectionManager.queue;
                        if (!queue)
                            throw new Error('queue is not defined');
                        channel.consume(queue.name, function (message) {
                            if (message) {
                                var m = new Message(message, channel);
                                _this.emit('message', m);
                            }
                        }, { noAck: false });
                        return [2 /*return*/];
                }
            });
        });
    };
    MessageConsumer.prototype.close = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connectionManager.close()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return MessageConsumer;
}(events_1.EventEmitter));
/**
 * The builder is used for the dependency injection.
 * When a publisher, a consumer or a subscriber is requested it will instatntiate a connection manager service object
 * and inject it into the requested service. It will return the instantiated service.
 */
var RMQBuilder = /** @class */ (function () {
    function RMQBuilder() {
    }
    /**
     * createFanoutPublisher() istanzia un MessagePublisher di tipo fanout che viene utilizzato per mandare messaggi ad un broker di RabbitMQ.
     * Un publisher con un exchange di tipo fanout manda i messaggi direttamente all'exchange senza assicurarsi che sia presente una coda per riceverli.
     * Se nessun subscriber si connette agganciando almeno una coda all'exchange i messaggi vengono persi
     * @param rmqHost broker host address
     * @param exchangeName the name of the exchange
     * @param options optional, exchange configuration object
     */
    RMQBuilder.createFanoutPublisher = function (rmqHost, exchangeName, options) {
        var exchange = {
            name: exchangeName,
            type: 'fanout',
            options: options || { durable: false }
        };
        var cm = new RMQConnectionManager(rmqHost, exchange);
        return new MessagePublisher(cm);
    };
    /**
     * createDirectPublisher() istanzia un MessagePublisher di tipo direct che viene utilizzato per mandare messaggi ad un broker di RabbitMQ.
     * Un publisher con un exchange di tipo direct manda i messaggi all'exchange che vengono inoltrati direttamente alla coda.
     * I messaggi pubblicati non vengono persi e cominciano a fluire dalla coda non appena un consumer viene collegato.
     * @param rmqHost broker host address
     * @param exchangeName the name of the exchange
     * @param queueName the name of the queue
     * @param exchangeOptions optional, exchange configuration object
     * @param queueOptions optional, queue configuration object
     */
    RMQBuilder.createDirectPublisher = function (rmqHost, exchangeName, queueName, exchangeOptions, queueOptions) {
        var exchange = {
            name: exchangeName,
            type: 'direct',
            options: exchangeOptions || { durable: true }
        };
        var queue = {
            name: queueName,
            options: queueOptions || { durable: true }
        };
        var cm = new RMQConnectionManager(rmqHost, exchange, queue);
        return new MessagePublisher(cm);
    };
    /**
     * createSubscriber() istanzia un MessageSubscriber che rimane in ascolto per i messaggi pubblicati da un publisher fanout.
     * All'exchange Viene collegata una coda con routing key scelta dal broker e con ack automatico.
     * Ad ogni messaggio ricevuto viene emesso un evento 'message' con il contenuto del messaggio.
     * @param rmqHost broker host
     * @param exchangeName the name of the exchange
     * @param options optional, exchange configuration object
     */
    RMQBuilder.createSubscriber = function (rmqHost, exchangeName, options) {
        var exchange = {
            name: exchangeName,
            type: 'fanout',
            options: options || { durable: false }
        };
        var cm = new RMQConnectionManager(rmqHost, exchange);
        return new MessageSubscriber(cm);
    };
    /**
     * createConsumer() istanzia un MessageConsumer che legge i messaggi pubblicati nella coda di un exchange di tipo 'direct'.
     * I messaggi sono esposti tramite un iteretatr, una volta consumato viene automaticamente inviato il segnale di ack.
     * @param rmqHost broker host
     * @param exchangeName the name of the exchange
     * @param queueName the name of the queue
     * @param exchangeOptions optional, exchange configuration object
     * @param queueOptions optional, queue configuration object
     */
    RMQBuilder.createConsumer = function (rmqHost, exchangeName, queueName, exchangeOptions, queueOptions) {
        var exchange = {
            name: exchangeName,
            type: 'direct',
            options: exchangeOptions || { durable: true }
        };
        var queue = {
            name: queueName,
            options: queueOptions || { durable: true }
        };
        var cm = new RMQConnectionManager(rmqHost, exchange, queue);
        return new MessageConsumer(cm);
    };
    return RMQBuilder;
}());
exports.RMQBuilder = RMQBuilder;
//# sourceMappingURL=index.js.map